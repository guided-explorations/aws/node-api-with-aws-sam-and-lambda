'use strict';
const AWS = require('aws-sdk');
const mysql = require('mysql');


const createResponse = (statusCode, data) => {
    return {
        statusCode: statusCode,
        body: JSON.stringify(data)
    }
};

exports.get = (event, context, callback) => {
    const { id } = event.pathParameters;
    if (id !== null) {
        const connection = mysql.createConnection({
            host     : process.env.RDS_HOSTNAME,
            user     : process.env.RDS_USERNAME,
            password : process.env.RDS_PASSWORD,
            database : "users"
        });
        
        connection.connect((err) => {
            if (err) {
            	console.log('error connecting: ' + err.stack);
            	return;
            }
            console.log('connected as id ' + connection.threadId + "\n");
            const query = `SELECT * FROM user_records WHERE user_id = ?`;
            connection.query(query, [id], (err, rows) => {
                if(err) {
                  connection.destroy()
                  console.log(err)
                  throw err;
                }
                connection.end();
                callback(null, createResponse(200, {data:rows}));
            });
        })
    }
};

exports.getByQuery = (event, context, callback) => {
    const connection = mysql.createConnection({
        host     : process.env.RDS_HOSTNAME,
        user     : process.env.RDS_USERNAME,
        password : process.env.RDS_PASSWORD,
        database : "users"
    });
    
    connection.connect((err) => {
        if (err) {
            console.log('error connecting: ' + err.stack);
            return;
        }
        console.log('connected as id ' + connection.threadId + "\n");
        let user = event["queryStringParameters"]['username'];
        let pass = event["queryStringParameters"]['pass'];
        const query = "SELECT * FROM user_records WHERE username='"+user+"' AND pass='"+pass+"'";
        connection.query(query, (err, rows) => {
            if(err) {
              connection.destroy()
              console.log(err)
              throw err;
            }
            connection.end();
            callback(null, createResponse(200, {data:rows}));
        });
    })
};

exports.put = (event, context, callback) => {

    console.log(`ADDED ITEM SUCCESSFULLY WITH id = ${event.pathParameters.id}`);
    callback(null, createResponse(200, null));

};

exports.delete = (event, context, callback) => {
    
    console.log(`DELETED ITEM SUCCESSFULLY WITH id = ${event.pathParameters.id}`);
    callback(null, createResponse(200, null));
};